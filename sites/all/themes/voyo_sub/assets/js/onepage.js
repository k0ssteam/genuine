/*
Onepage Menu
http://www.drupalexp.com/
*/


(function ($) {

  $.fn.dexpOnepage = function (options) {
    var settings = $.extend({
      'scrollSpeed': 500,
      'prefix': '',
      'offsetTop': 0,
    }, options);
    return this.each(function () {
      var $this = $(this);
      var $elements = [];
      var srollevent = true;
      $this.find('a[href*=#]').once('onepage-menu', function(){
        var id = settings.prefix + $(this).attr('href').split('#')[1];
        if($('#'+id).length > 0){
          var element = {element:$('#'+id), menu: $(this)};
          $elements.push(element);
          $(this).click(function(e){
            e.preventDefault();
            var goTo = element.element.offset().top - settings.offsetTop;
            srollevent = false;
            $("html, body").stop().animate({ scrollTop: goTo }, settings.scrollSpeed, function(){
              setTimeout(function(){srollevent = true;},100);
            });
            $this.find('a').removeClass('active');
            $(this).addClass('active');
          })
        }else if(id == settings.prefix+'home' || id == settings.prefix){
          $(this).click(function(){
            srollevent = false;
            $("html, body").stop().animate({ scrollTop: 0 }, settings.scrollSpeed, function(){
              setTimeout(function(){srollevent = true;},100);
            });
            $this.find('a').removeClass('active');
            $(this).addClass('active');
          });
        }
      });

      $(window).scroll(function(){
        if(srollevent == false) return;
        var window_top = $(window).scrollTop() + settings.offsetTop;
        if(window_top == settings.offsetTop){
          $this.find('a[href*=#]').removeClass('active');
          $this.find('a[href=#home],a[href=#]').addClass('active');
        }else{
          $($elements).each(function(){
            if(this.element.offset().top <= window_top){
              $this.find('a[href*=#]').removeClass('active');
              this.menu.addClass('active');
            }
          });
        }
      });
    });
  }
})(jQuery);
