(function ($) {
  Drupal.behaviors.voyo_theme = {
    attach: function (context, settings) {
      if ($('.view-products-shop .dexp-grid-items').attr('class') && $('.view-products-shop .dexp-grid-item').attr('class')) {
        var classGridList = $('.view-products-shop .dexp-grid-items').attr('class').split(/\s+/);
        var gridClasses = "";
        $.each(classGridList, function (index, item) {
          if (item.match("grid-")) {
            gridClasses = gridClasses + item + " ";
          }
        });

        var colClassList = $('.view-products-shop .dexp-grid-item').attr('class').split(/\s+/);
        var colClasses = "";
        $.each(colClassList, function (index, item) {
          if (item.match("col-")) {
            colClasses = colClasses + item + " ";
          }
        });
        $('.shop-view .list-view').click(function () {
          if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.grid-view').removeClass('active');
          }
          if ($('.view-products-shop').hasClass('product-grid-view')) {
            $('.view-products-shop').addClass('product-list-view');
            $('.view-products-shop').removeClass('product-grid-view');
            $('.view-products-shop .dexp-grid-items').removeClass(gridClasses);
            $('.view-products-shop .dexp-grid-item').removeClass(colClasses);
            $('.view-products-shop .dexp-grid-item').addClass('dexp-list-item');
            $('.view-products-shop .dexp-grid-item').removeClass('dexp-grid-item');
            $('.grid-wrap').hide();
            $('.list-wrap').show();
          }
        });
        $('.shop-view .grid-view').click(function () {
          if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.list-view').removeClass('active');
          }
          if ($('.view-products-shop').hasClass('product-list-view')) {
            $('.view-products-shop').removeClass('product-list-view');
            $('.view-products-shop').addClass('product-grid-view');

            $('.view-products-shop .dexp-list-item').addClass('dexp-grid-item');
            $('.view-products-shop .dexp-list-item').removeClass('dexp-list-item');
            $('.view-products-shop .dexp-grid-items').addClass(gridClasses);
            $('.view-products-shop .dexp-grid-item').addClass(colClasses);
            $('.list-wrap').hide();
            $('.grid-wrap').show();
          }
        });
      }

      $('.search-toggle').once('search', function () {
        $(this).click(function () {
          $('body').toggleClass('search-open');
        });
      });
      /* Fix column sibling height */
      $(".data-height-fix").each(function () {
        var siblingHeight = $(this).find($(".get-height")).outerHeight();
        $(".set-height").css("height", siblingHeight);
      });
      //$("a[rel^='prettyPhoto']").prettyPhoto();
      /* Tooltips */
      if ($().tooltip) {
        $("[data-toggle='tooltip']").tooltip();
      }
      /* Popovers */
      if ($().popover) {
        $("[data-toggle='popover']").popover();
      }
      Drupal.settings.voyo = Drupal.settings.voyo || {};
      Drupal.settings.voyo.header_overlay_logo = Drupal.settings.voyo.header_overlay_logo || '';
      if (Drupal.settings.voyo.header_overlay_logo != '' && $('body').hasClass('header-overlay')) {
        $('a.site-logo').once('logo', function () {
          $('a.site-logo img').addClass('primary');
          $(this).append('<img class="secondary" src="' + Drupal.settings.voyo.header_overlay_logo + '"/>');
        });
      }
      /* One Page menu */
      $('.block-dexp-menu.onepage .dexp-menu').once('onepage', function () {
        $(this).dexpOnepage({
          prefix: 'section-',
          offsetTop: 80
        })
      });
      /* Kenburn background */
      $('.kenburn-bg').once('kenburn', function () {
        var $this = $(this);
        setInterval(function () {
          var backgroundPositionX = $this.data('bg-position-x') || 0;
          $this.css({
            backgroundPosition: backgroundPositionX + 'px top'
          });
          backgroundPositionX--;
          $this.data('bg-position-x', backgroundPositionX);
        }, 20);
      });
    }
  }
  var equalizer = function () {
    $(".equalizer").each(function () {
      var getHeight = $(this).find($(".get-height")).height();
      $(".set-height").css("height", getHeight);
    });
  }
  $(window).load(function () {
    equalizer();
  });
})(jQuery);

jQuery(document).ready(function ($) {
  "use strict";

  /*Mail chimp text value default*/
  $(".mailchimp-signup-subscribe-form input[name='mergevars[EMAIL]']").val("E-mail Address");

  $('#addtocart').click(function (event) {
    event.preventDefault();
    $(this).next().next().find("form").submit();
  });
  $('#addtocartlist').click(function (event) {
    event.preventDefault();
    $(this).next().find("form").submit();
  });

  /*Go to top*/
  $(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
      $('.back-to-top').css({bottom:"15px"});
      } else {
        $('.back-to-top').css({bottom:"-100px"});
      }
  });
  $('.back-to-top').click(function () {
    $('html, body').animate({
      scrollTop: '0px'
    }, 800);
     return false;
  });
  
  /*Preloader*/
  $(window).load(function(){
    if ($("#preloader").length) {
      $('#status').fadeOut(); 
      $('#preloader').delay(350).fadeOut('slow');
      $('body').delay(350).css({'overflow':'visible'});
    }
  });
  setTimeout(function(){
    if ($("#preloader").length) {
      $('#status').fadeOut();
      $('#preloader').delay(350).fadeOut('slow');
      $('body').delay(350).css({'overflow':'visible'});
    }
  }, 15000);
});
