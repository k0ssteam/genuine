<?php
  $backgrounds = array('bg1','bg2','bg3','bg4','bg5','bg6','bg7','bg8', 'bg9','bg10');
  $current_path = current_path();
  drupal_add_library('system', 'ui.slider');
?>
<span class="quicksettings_toggle fa fa-cogs"></span>
<div class="dexp_settings">
  <h3>Layout Style</h3>
  <select class="dexp_layout form-select" name="setting_layout">
    <option value="wide">Wide</option>
    <option value="boxed">Boxed</option>
  </select>
  <h3>Page width: <span class="page-width">1170px</span></h3>
  <div id="slider"></div>
  <h3>Header</h3>
  <select class="dexp_header form-select" name="setting_header">
    <option value="" selected>Default</option>
    <option value="header-color">Color</option>
    <option value="header-overlay">Overlay</option>
  </select>
  <!--
  <div class="header-opacity">
    <h3>Header Opacity: <span class="header-opacity"><?php print theme_get_setting('header_overlay_opacity');?></span></h3>
    <div id="slider-header"></div>
  </div>
  -->
  <h3>Direction</h3>
  <select class="dexp_direction form-select" name="setting_direction">
    <option value="ltr">LTR</option>
    <option value="rtl">RTL</option>
  </select>
  <h3>Predefined Colors</h3>
  <ul class="presets">
    <?php foreach ($presets as $k => $preset): ?>
    <li class="<?php print drupal_html_class($preset->key);?>"><?php print l('<span style="background-color:' . $preset->link_color . '"></span>', 'drupalexp/preset/' . ($k + 1), array('html' => true,'query'=>array('destination'=>$current_path))); ?></li>
    <?php endforeach; ?>
  </ul>
  <h3>Background</h3>
  <ul class="dexp_background">
    <?php foreach ($backgrounds as $background): ?>
      <li><span class="<?php print $background;?>"></span></li>
    <?php endforeach; ?>
  </ul>
  <div class="clearfix"></div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function($){
    if($('body').hasClass('header-overlay')){
      $('[name=setting_header]').val('header-overlay');
    }
    $( "#slider" ).slider({
      value:1170,
      min: 960,
      max: 1170,
      step: 1,
      slide: function( event, ui ) {
        $('span.page-width').text(ui.value +'px');
        $('.container').css({maxWidth:ui.value});
        if($('body').hasClass('boxed')){
          $('.dexp-body-inner').css({maxWidth:ui.value});
        }
        Drupal.attachBehaviors();
      }
    });
    setTimeout(function(){
      $('#block-dexp-quicksettings-dexp-quicksettings').removeClass('open');
    },3000);
    $('.quicksettings_toggle').click(function(){
      $('#block-dexp-quicksettings-dexp-quicksettings').toggleClass('open');
    })
    $('select.dexp_layout').change(function(){
      $('body').removeClass('boxed wide').addClass($(this).val());
      $(window).trigger('resize');
    });
    if($('body').hasClass('boxed')){
      $('select.dexp_layout').val('boxed').trigger('change');
    }else{
      $('select.dexp_layout').val('wide').trigger('change');
    }
    $('select.dexp_direction').change(function(){
      $('body').removeClass('ltr rtl').addClass($(this).val());
    });
    $('select.dexp_header').change(function(){
      $('body').removeClass('header-overlay header-color').addClass($(this).val());
      Drupal.attachBehaviors();
      $("html, body").animate({ scrollTop: 0 }, 800);
    });
    $('ul.dexp_background span').click(function(){
      if($('select.dexp_layout').val()=='wide'){
        alert('Please select boxed layout');
        return;
      }
      $('body').removeClass('bg1 bg2 bg3 bg4 bg5 bg6 bg7 bg8 bg9 bg10').addClass($(this).attr('class'));
    })
  })
</script>
