<?php
$icon_class='';
if($icon !==''){
    $icon_class="<i class='".$icon."'></i> ";
}
?>
<div id="<?php print $element_id; ?>" class="skill-bar <?php print $class; ?>" data-percent="<?php print $percent."%"; ?>">
	<h6 class="skill-bar-label"><?php print $icon_class . $content;?></h6>
    <div class="progress">
        <div class="progress-bar"  role="progressbar" aria-valuenow="<?php print $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">            
		<small class="skill-bar-percent"><?php print $percent . "%";?></small>
        </div>
    </div>	
</div>