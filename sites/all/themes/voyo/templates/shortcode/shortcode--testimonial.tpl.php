<div class="item <?php if ($sequence == 0) print 'active'; ?>">
    <div class="testimonials-content">
        <?php print $content; ?>
    </div>
    <?php if ($image != ''): ?><div class="testimonial-image"><img alt="" src="<?php print $image; ?>"></div><?php endif; ?>
    <div class="person-says">
        <span><?php print $name . " - " . $position; ?></span>
    </div>
</div>
