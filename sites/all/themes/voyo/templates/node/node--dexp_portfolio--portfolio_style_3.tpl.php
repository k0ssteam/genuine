<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
      <div class="content" <?php print $content_attributes; ?>>
        <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_image']);
    ?>
          <div class="portfolio-image">
            <?php print render($content['field_image']);?>
              <div class="portfolio-info1"> <a class="info" href="<?php print $node_url;?>"><i class="fa fa-link"></i></a></div>
          </div>

          <div class="portfolio-info">
            <h2 class="portfolio-info-title"><?php print $title;?></h2>
            <div class="blog-body">
              <?php print render($content['body']); ?>
            </div>

          </div>
      </div>
</div>
