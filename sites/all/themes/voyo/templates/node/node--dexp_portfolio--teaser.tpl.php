<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php print render($title_suffix); ?>
  <div class="content" <?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_image']);
    ?>
      <div class="portfolio-image">
        <?php print render($content['field_image']);?>
      </div>
      <div class="portfolio-info">
        <div class="inner">
          <h3 class="portfolio-title"><?php print $title;?></h3>
          <div class="blog-body">
            <?php print render($content['body']); ?>
          </div>
          <a class="info" href="<?php print $node_url;?>"><i class="fa fa-link"></i></a>
        </div>
      </div>
  </div>
</div>
