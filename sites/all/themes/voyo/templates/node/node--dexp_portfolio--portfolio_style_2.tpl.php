<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
  <div class="content" <?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_image']);
      $lightboxrel = 'portfolio_'.$node->nid;
        $portfolio_images = field_get_items('node', $node, 'field_image');
        $first_image = '';
        if ($portfolio_images) {
            foreach ($portfolio_images as $k => $portfolio_image) {
                $first_image = file_create_url($portfolio_image['uri']);
                break;
            }
        }
    ?>
      <div class="portfolio-image">
        <?php print render($content['field_image']);?>
      </div>
      <div class="portfolio-info">
        <div class="portfolio-info-bordered">
          <div class="portfolio-in-inner">
          <a title="<?php print $title; ?>" rel="prettyPhoto[<?php print $lightboxrel; ?>]" href="<?php print $first_image; ?>"><i class="fa fa-search"></i></a>          
          <a href="<?php print $node_url;?>"><i class="fa fa-link"></i></a>
        </div>
        </div>
      </div>
  </div>

</div>
