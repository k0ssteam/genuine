<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1">
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <link rel="stylesheet" href="../sites/all/modules/travel/css/travel.css">
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <?php if(theme_get_setting('preload')):?>
    <!-- loader -->
    <div id="preloader">
      <div id="status">&nbsp;</div>
    </div>
    <!-- /loader -->
    <?php endif;?>
    <div id="skip-link">
      <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    </div>
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
    <div class="back-to-top"><i class="fa fa-angle-up fa-2x"></i></div>
  </body>
</html>
