<?php
$style = theme_get_setting('blog_style');
if ($style == 'large') {
   print views_embed_view('blog', 'page_large');
} else {
  if ($style == 'grid') {
    print views_embed_view('blog', 'page_grid');
  } else {
    print views_embed_view('blog', 'page_medium');
  }
}
