<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
global $user;
if(module_exists('commerce')):
$quantity = 0;
$order = commerce_cart_order_load($user->uid);
if ($order) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $line_items = $wrapper->commerce_line_items;
  $quantity = commerce_line_items_quantity($line_items, commerce_product_line_item_types());
  $order_total = commerce_line_items_total($line_items);
}
?>
<a href="<?php print url('cart'); ?>" title="<?php print t('View cart');?>">
  <span class="count"><?php print $quantity?></span>
  <i class="fa fa-shopping-cart"></i>
</a>
<?php endif;?>
