<div data-percent="<?php print $content['field_percent']['#items'][0]['value']?>%" class="skill-bar " id="skill-bar">
	<h6 class="skill-bar-label"><i class="fa fa-<?php print $content['field_icon']['#items'][0]['value']?>"></i> <?php print $content['field_title']['#items'][0]['value']?> <?php print $content['field_percent']['#items'][0]['value']?>%</h6>
    <div class="progress">
        <div style="width: <?php print $content['field_percent']['#items'][0]['value']?>%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="85" role="progressbar" class="progress-bar">
		<small class="skill-bar-percent"><?php print $content['field_percent']['#items'][0]['value']?>%</small>
        </div>
    </div>
</div>
