<?php 
  $node = $element['#object'];
  $view_mode = $element['#view_mode'];
  //print $view_mode;
  $action_teaser = theme_get_setting('portfolio_teaser_image_action');
 // print  $action_teaser;
  $action_full = theme_get_setting('portfolio_full_image_action');
?>
<?php if ($view_mode != 'default' && $view_mode != 'full'):?> <!-- view mode teaser -->
  <?php if(count($items) <= 1):?>
  <div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
  <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
    <?php if ($action_teaser == 'url') :?>
    asasa
      <a href="<?php print drupal_lookup_path('alias',"node/".$node->nid);?>" title="">
        <?php print render($item); ?>
      </a>
    <?php elseif ($action_teaser == 'pretty') :?>
      <?php 
        $path_image = "";
        if (isset($item['#item']['uri'])) {
          $path_image = file_create_url($item['#item']['uri']); 
        }
        $prettyRel = 'blog_' . $node->nid;
      ?>
       <a rel="prettyPhoto[<?php echo $prettyRel;?>]" title="" href="<?php print $path_image ;?>">
          <?php print render($item); ?>
       </a>  
    <?php else: print render($item); endif;?>
  </div>
  <?php endforeach; ?>
  </div>
  </div>
  <?php else:
    $carousel_id = drupal_html_id('dexp_carousel');
  ?>
  <div id="<?php print $carousel_id;?>" class="carousel slide dexp_carousel <?php print $classes; ?>"<?php print $attributes; ?> data-ride="carousel"
    <?php if (theme_get_setting('auto_slide_setting') == 'no'):?> data-interval="false" <?php endif;?>>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <?php foreach ($items as $delta => $item): ?>
      <div class="item field-item <?php print $delta == 0?'active':'';?>"<?php print $item_attributes[$delta]; ?>>
        <?php if ($action_teaser == 'url') :?>
          <a href="<?php print drupal_lookup_path('alias',"node/".$node->nid);?>" title="">
            <?php print render($item); ?>
          </a>
        <?php elseif($action_teaser == 'pretty') :?>
          <?php 
            $path_image = "";
            if (isset($item['#item']['uri'])) {
              $path_image = file_create_url($item['#item']['uri']); 
            }
            $prettyRel = 'blog_' . $node->nid;
          ?>
           <a rel="prettyPhoto[<?php echo $prettyRel;?>]" title="" href="<?php print $path_image;?>">
              <?php print render($item); ?>
           </a>  
        <?php else: print render($item); endif;?> 
      </div>
      <?php endforeach; ?>
    </div>

    <!-- Carousel indicators -->
    <?php if (theme_get_setting('pager_setting') == 'yes'):?>
      <ol class="carousel-indicators">
        <?php foreach ($items as $delta => $item): ?>
          <li data-target="#<?php print $carousel_id; ?>" data-slide-to="<?php print $delta; ?>" <?php print $delta == 0 ? 'class=active' : ''; ?>></li>
        <?php endforeach; ?>
       </ol>   
    <?php endif;?>

    <!-- Controls -->
    <?php if (theme_get_setting('navigation_setting') == 'yes'):?>
    <a class="left carousel-control" href="#<?php print $carousel_id;?>" data-slide="prev">
      <span class="fa fa-angle-left"></span>
    </a>
    <a class="right carousel-control" href="#<?php print $carousel_id;?>" data-slide="next">
      <span class="fa fa-angle-right"></span>
    </a>
    <?php endif;?>
  </div>
  <?php endif;?>
<?php else: ?> <!-- view mode full -->
  <?php if(count($items) <= 1):?>
  <div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
  <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
  <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
    <?php if ($action_full == 'url') :?>
      <a href="<?php print drupal_lookup_path('alias',"node/".$node->nid);?>" title="">
        <?php print render($item); ?>
      </a>
    <?php elseif ($action_full == 'pretty') :?>
      <?php 
        $path_image = "";
        if (isset($item['#item']['uri'])) {
          $path_image = file_create_url($item['#item']['uri']); 
        }
        $prettyRel = 'blog_' . $node->nid;
      ?>
       <a rel="prettyPhoto[<?php echo $prettyRel;?>]" title="" href="<?php print $path_image ;?>">
          <?php print render($item); ?>
       </a>  
    <?php else: print render($item); endif;?>
  </div>
  <?php endforeach; ?>
  </div>
  </div>
  <?php else:
    $carousel_id = drupal_html_id('dexp_carousel');
  ?>
  <div id="<?php print $carousel_id;?>" class="carousel slide dexp_carousel <?php print $classes; ?>"<?php print $attributes; ?> data-ride="carousel"
  <?php if (theme_get_setting('auto_slide_setting') == 'no'):?> data-interval="false" <?php endif;?>>
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php foreach ($items as $delta => $item): ?>
    <div class="item field-item <?php print $delta == 0?'active':'';?>"<?php print $item_attributes[$delta]; ?>>
      <?php if ($action_full == 'url') :?>
        <a href="<?php print drupal_lookup_path('alias',"node/".$node->nid);?>" title="">
          <?php print render($item); ?>
        </a>
      <?php elseif($action_full == 'pretty') :?>
        <?php 
          $path_image = "";
          if (isset($item['#item']['uri'])) {
            $path_image = file_create_url($item['#item']['uri']); 
          }
          $prettyRel = 'blog_' . $node->nid;
        ?>
         <a rel="prettyPhoto[<?php echo $prettyRel;?>]" title="" href="<?php print $path_image;?>">
            <?php print render($item); ?>
         </a>  
      <?php else: print render($item); endif;?> 
    </div>
    <?php endforeach; ?>
  </div>
  
  <!-- Carousel indicators -->
  <?php if (theme_get_setting('pager_setting') == 'yes'):?>
    <ol class="carousel-indicators">
      <?php foreach ($items as $delta => $item): ?>
        <li data-target="#<?php print $carousel_id; ?>" data-slide-to="<?php print $delta; ?>" <?php print $delta == 0 ? 'class=active' : ''; ?>></li>
      <?php endforeach; ?>
     </ol>   
  <?php endif;?>
  
  <!-- Controls -->
  <?php if (theme_get_setting('navigation_setting') == 'yes'):?>
  <a class="left carousel-control" href="#<?php print $carousel_id;?>" data-slide="prev">
    <span class="fa fa-angle-left"></span>
  </a>
  <a class="right carousel-control" href="#<?php print $carousel_id;?>" data-slide="next">
    <span class="fa fa-angle-right"></span>
  </a>
  <?php endif;?>
  </div>
  <?php endif;?>
<?php endif;

