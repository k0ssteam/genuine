<?php
/**
 * Implements hook_form_system_theme_settings_alter()
 */
function voyo_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL) {
  $theme_key = arg(3);
  $form['drupalexp_settings'] = array(
      '#type' => 'vertical_tabs',
  );
  $form['voyo_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Voyo'),
      '#group' => 'drupalexp_settings',
      '#weight' => 3,
  );
  $form['voyo_settings']['preload'] = array(
      '#type' => 'select',
      '#title' => t('Enable Preloader'),
      '#options' => array(0=>'No', 1=>'Yes'),
      '#default_value' => theme_get_setting('preload'),
  );
  $form['voyo_settings']['header_settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Header settings',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['voyo_settings']['header_settings']['header_style'] = array(
      '#type' => 'select',
      '#title' => t('Header Style'),
      '#description' => t('Provide header style for all pages'),
      '#options' => array(
        '' => t('Header White'),
        'header-color' => t('Header Color'),
        'header-overlay' => t('Header Overlay'),
      ),
      '#default_value' => theme_get_setting('header_style'),
  );
  /*
  $form['voyo_settings']['header_settings']['header_overlay_opacity'] = array(
      '#type' => 'textfield',
      '#title' => t('Header Overlay Opacity'),
      '#description' => t('Define Header Overlay transparent (from 0 to 1)'),
      '#default_value' => theme_get_setting('header_overlay_opacity'),
      '#states' => array(
          'visible' => array(
            '[name=header_style]' => array('value' => 'header-overlay'),
          ),
      ),
  );
  */
  $form['voyo_settings']['carousel_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Field Image Carousel settings',
    '#description' => t('Configure carousel for image field in content type Portoflio, Blog, Product...'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['voyo_settings']['carousel_settings']['auto_slide_setting'] = array(
      '#type' => 'select',
      '#title' => t('Auto Slide'),
      '#description' => t('Auto move to next image in image carousel'),
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
    '#default_value' => theme_get_setting('auto_slide_setting'),
  );
  $form['voyo_settings']['carousel_settings']['navigation_setting'] = array(
      '#type' => 'select',
      '#title' => t('Show Next/Pre Navigation'),
      '#description' => t('Show Next/Previous button on images carousel when hovering'),
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
    '#default_value' => theme_get_setting('navigation_setting'),
  );
  $form['voyo_settings']['carousel_settings']['pager_setting'] = array(
      '#type' => 'select',
      '#title' => t('Show Pager Control'),
      '#description' => t('Show dot cirlce on images carousel'),
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
    '#default_value' => theme_get_setting('pager_setting'),
  );
  
  $form['voyo_settings']['portfolio_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Portfolio settings',
    '#description' => t('Configure for porfolio pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['voyo_settings']['portfolio_settings']['portfolio_teaser_image_action'] = array(
      '#type' => 'select',
      '#title' => t('Click on Image in grid will'),
      '#description' => t('Action when user click on image grid view'),
      '#options' => array(
        '' => t('Do nothing'),
        'url' => t('Link to content'),
        'pretty' => t('Open Pretty Photo (same Lightbox)'),
      ),
    '#default_value' => theme_get_setting('portfolio_teaser_image_action'),
  );
  $form['voyo_settings']['portfolio_settings']['portfolio_full_image_action'] = array(
      '#type' => 'select',
      '#title' => t('Click on Image in product detail page'),
      '#description' => t('Action when user click on image detail page'),
      '#options' => array(
        '' => t('Do nothing'),
        'pretty' => t('Open Pretty Photo (same Lightbox)'),
      ),
    '#default_value' => theme_get_setting('portfolio_full_image_action'),
  );
  
  $form['voyo_settings']['blog_settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Blog settings',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['voyo_settings']['blog_settings']['blog_style'] = array(
      '#type' => 'select',
      '#title' => t('Blog Style'),
      '#description' => t('Set blog style for blog path (Ex: http://yoursiteURL.com/blog)'),
      '#options' => array(
        'large' => t('Large Image Style'),
        'medium' => t('Medium Image Style'),
        'grid' => t('Grid Style'),
      ),
    '#default_value' => theme_get_setting('blog_style'),
  );
  $form['voyo_settings']['blog_settings']['blog_teaser_image_action'] = array(
      '#type' => 'select',
      '#title' => t('Click on Image in grid will'),
      '#description' => t('Action when user click on image teaser view'),
      '#options' => array(
        '' => t('Do nothing'),
        'url' => t('Link to content'),
        'pretty' => t('Open Pretty Photo (same Lightbox)'),
      ),
    '#default_value' => theme_get_setting('blog_teaser_image_action'),
  );
  $form['voyo_settings']['blog_settings']['blog_full_image_action'] = array(
      '#type' => 'select',
      '#title' => t('Click on Image in Blog detail page'),
      '#description' => t('Action when user click on image detail page'),
      '#options' => array(
        '' => t('Do nothing'),
        'pretty' => t('Open Pretty Photo (same Lightbox)'),
      ),
    '#default_value' => theme_get_setting('blog_full_image_action'),
  );
  
  $form['voyo_settings']['product_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Product settings',
    '#description' => t('Configure for shop pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['voyo_settings']['product_settings']['product_teaser_image_action'] = array(
      '#type' => 'select',
      '#title' => t('Click on Image in grid will'),
      '#description' => t('Action when user click on image grid view'),
      '#options' => array(
        '' => t('Do nothing'),
        'url' => t('Link to content'),
        'pretty' => t('Open Pretty Photo (same Lightbox)'),
      ),
    '#default_value' => theme_get_setting('product_teaser_image_action'),
  );
  $form['voyo_settings']['product_settings']['product_full_image_action'] = array(
      '#type' => 'select',
      '#title' => t('Click on Image in product detail page'),
      '#description' => t('Action when user click on image detail page'),
      '#options' => array(
        '' => t('Do nothing'),
        'pretty' => t('Open Pretty Photo (same Lightbox)'),
      ),
    '#default_value' => theme_get_setting('product_full_image_action'),
  );
}
