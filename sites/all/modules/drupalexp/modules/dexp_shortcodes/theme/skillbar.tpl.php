<?php
$icon_class='';
if($icon !==''){
    $icon_class="<i class='".$icon."'></i> ";
}
?>
<div id="<?php print $element_id; ?>" class="skill-bar" data-percent="<?php print $percent."%"; ?>">
    <div class="progress <?php print $class; ?>">
        <div class="progress-bar"  role="progressbar" aria-valuenow="<?php print $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span>
			<?php
            print $icon_class . $content .' '. $percent . "%";
            ?>
			</span>
        </div>
    </div>
</div>