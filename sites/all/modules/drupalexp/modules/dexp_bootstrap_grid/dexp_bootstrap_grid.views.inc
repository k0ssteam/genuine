<?php

/**
 * Implements hook_views_plugins
 */
function dexp_bootstrap_grid_views_plugins() {
  return array(
      'style' => array(
          'dexp_bootstrap_grid' => array(
              'title' => t('Dexp Bootstrap Grid'),
              'help' => t('Display content in a bootstrap responsive grid.'),
              'handler' => 'dexp_bootstrap_grid_plugin_style_grid',
              'uses options' => TRUE,
              'uses row plugin' => TRUE,
              'uses row class' => TRUE,
              'type' => 'normal',
              'theme' => 'views_dexp_bootstrap_grid',
          ),
      ),
  );
}