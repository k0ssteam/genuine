<?php

/**
 * @file
 * Defines the style plugin for the Dexp Bootstrap Grid
 */
class dexp_bootstrap_grid_plugin_style_grid extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    $options['grid_cols_lg'] = array('default' => 3);
    $options['grid_cols_md'] = array('default' => 3);
    $options['grid_cols_sm'] = array('default' => 6);
    $options['grid_cols_xs'] = array('default' => 12);
    $options['grid_margin'] = array('default' => 10);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['grid_cols_lg'] = array(
        '#type' => 'select',
        '#title' => t('Large Desktop Columns'),
        '#options' => array(1=>'1',2=>'2',3=>'3',4=>'4',6=>'6',12=>'12'),
        '#default_value' => $this->options['grid_cols_lg']
    );
	$form['grid_cols_md'] = array(
        '#type' => 'select',
        '#title' => t('Desktop Columns'),
        '#options' => array(1=>'1',2=>'2',3=>'3',4=>'4',6=>'6',12=>'12'),
        '#default_value' => $this->options['grid_cols_md']
    );
	$form['grid_cols_sm'] = array(
        '#type' => 'select',
        '#title' => t('Tablet Columns'),
        '#options' => array(1=>'1',2=>'2',3=>'3',4=>'4',6=>'6',12=>'12'),
        '#default_value' => $this->options['grid_cols_sm']
    );
	$form['grid_cols_xs'] = array(
        '#type' => 'select',
        '#title' => t('Phone Columns'),
        '#options' => array(1=>'1',2=>'2',3=>'3',4=>'4',6=>'6',12=>'12'),
        '#default_value' => $this->options['grid_cols_xs']
    );
  }
}
